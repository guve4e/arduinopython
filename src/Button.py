#!/usr/bin/python
# This script is listening for serial event
# if bits received, it sends HTTP request
# to Google API

import serial
import Rest
import Tools


# make a read
try:
    ser = serial.Serial(Tools.PORT, Tools.BAUD)

except IOError:
    print("Can not open port! " + Tools.PORT)


# read stream
while True:
    data_raw = ser.readline()
    #print(data_raw)

    res = Rest.RestCall('android.googleapis.com','/gcm/send','POST',Tools.headers,Tools.data)
    res.makePost(Tools.data, Tools.headers)
