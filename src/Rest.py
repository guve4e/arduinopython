import json
import http.client


class RestCall:
    # Class makes HTTP Request

    def __init__(self, url, controller, method, headers, data):
        # Constructor
        self.url = url
        self.controller = controller
        self.method = method
        self.headers = headers
        self.data = data

    def makePost(self, data, headers):
        # makes post request

        try:
            # make json
            json_data = json.dumps(data)
            # need to create a TCP connection that you will use to communicate with the remote server
            conn = http.client.HTTPConnection(self.url)
            # then send HTTP request over HTTPS connection
            # choose method, parameters (controllers) data and headers
            conn.request(self.method, self.controller, json_data, headers)
            # get response
            response = conn.getresponse()
            # debug
            # print response
            print(response.status, response.reason)
            print(response.read().decode())


        except Exception as e:
            print("EXCEPTION")
            print(type(e))
